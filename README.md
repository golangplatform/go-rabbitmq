# go-rabbitmq

This repository holds all POC's related to GoLang and RabbitMQ.

# Installation on Windows 10
- RabbitMQ requires a 64-bit supported version of Erlang for Windows to be installed. 
- Latest binary builds for Windows can be obtained from the Erlang/OTP Version Tree page.
- Note that Erlang must be installed using an administrative account or it won't be discoverable to the RabbitMQ Windows service.
- Once a Erlang is installed, download the RabbitMQ installer, "rabbitmq-server-{version}.exe" and run it
- It installs RabbitMQ as a Windows service and starts it using the default configuration.
- Once both Erlang and RabbitMQ have been installed, a RabbitMQ node can be started as a Windows service. 
- The RabbitMQ service starts automatically. RabbitMQ Windows service ca be managed from the Start menu.
- RabbitMQ nodes are often managed, inspected and operated using CLI Tools in PowerShell.
- On Windows, CLI tools have a .bat suffix compared to other platforms. For example, rabbitmqctl on Windows is invoked as rabbitmqctl.bat or open RabbitMQ command prompt(sbin dir).
- See more at https://www.rabbitmq.com/install-windows.html

# Default User Access
- The broker creates a user guest with password guest. 
- Unconfigured clients will in general use these credentials. 
- By default, these credentials can only be used when connecting to the broker as localhost so you will need to take action before connecting from any other machine

# RabbitMQ management plugin
- See more at https://www.rabbitmq.com/management.html

# RabbitMQ/AMQP Concepts
- See more at https://www.rabbitmq.com/tutorials/amqp-concepts.html

# Hello RabbitMQ tutorial
- See mopre at https://www.rabbitmq.com/tutorials/tutorial-one-go.html

# For Production
- https://www.rabbitmq.com/confirms.html
- https://www.rabbitmq.com/production-checklist.html
- https://www.rabbitmq.com/monitoring.html

# Useful commands
- If you want to check on the queue, try using command rabbitmqctl list_queues