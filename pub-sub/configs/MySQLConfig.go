package configs

type Config struct {
	DB *DBConfig
}

type DBConfig struct {
	Dialect  string
	Username string
	Password string
	Name     string
	Charset  string
}

func GetMySQLConfig() *Config {
	// return &Config{
	// 	DB: &DBConfig{
	// 		Dialect:  os.Getenv("MYSQL_DIALECT"),
	// 		Username: os.Getenv("MYSQL_USER"),
	// 		Password: os.Getenv("MYSQL_PASSWORD"),
	// 		Name:     os.Getenv("MYSQL_DBNAME"),
	// 		Charset:  os.Getenv("MYSQL_CHARSET"),
	// 	},
	// }

	return &Config{
		DB: &DBConfig{
			Dialect:  "mysql",
			Username: "root",
			Password: "root",
			Name:     "pub-sub-assignment",
			Charset:  "utf8",
		},
	}
}
