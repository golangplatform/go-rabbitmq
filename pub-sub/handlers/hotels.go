package handlers

import (
	"encoding/json"
	"net/http"
	"pub-sub/models"

	"github.com/jinzhu/gorm"
)

// Get list of hotels
func GetHotels(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	hotels := []models.Hotel{}
	db.Find(&hotels)
	respondJSON(w, http.StatusOK, hotels)
}

// Add hotel
func AddHotel(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	hotel := models.Hotel{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&hotel)
	if err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	err = db.Save(&hotel).Error
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, hotel)
}
