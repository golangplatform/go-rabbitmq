package handlers

import (
	"encoding/json"
	"net/http"
	"pub-sub/models"

	"github.com/jinzhu/gorm"
)

// Get list of rate plans
func GetRatePlans(db *gorm.DB, w http.ResponseWriter, r *http.Request) {

	ratePlans := []models.RatePlan{}
	db.Find(&ratePlans)
	respondJSON(w, http.StatusOK, ratePlans)
}

// Add Rate Plan
func AddRatePlan(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	ratePlan := models.RatePlan{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&ratePlan)
	if err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}

	defer r.Body.Close()

	err = db.Save(&ratePlan).Error
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, ratePlan)
}
