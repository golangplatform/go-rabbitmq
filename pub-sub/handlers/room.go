package handlers

import (
	"encoding/json"
	"net/http"
	"pub-sub/models"

	"github.com/jinzhu/gorm"
)

// Get list of rooms
func GetRooms(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	rooms := []models.Room{}
	db.Find(&rooms)
	respondJSON(w, http.StatusOK, rooms)
}

// Add room
func AddRoom(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	room := models.Room{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&room)
	if err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()
	err = db.Save(&room).Error
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusCreated, room)
}
