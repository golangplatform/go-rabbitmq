package main

import (
	"pub-sub/configs"
	"pub-sub/routes"
)

func main() {
	config := configs.GetMySQLConfig()
	app := &routes.App{}
	app.Initialize(config)
	app.Run(":3000")
}
