package models

import (
	"database/sql/driver"
	"encoding/json"
	"time"

	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Hotel struct {
	HotelID     string    `gorm:"primaryKey" json:"hotel_id"`
	Name        string    `json:"name"`
	Country     string    `json:"country"`
	Address     string    `json:"address"`
	Latitude    float64   `json:"latitude"`
	Longitude   float64   `json:"longitude"`
	Telephone   string    `json:"telephone"`
	Amenities   Amenities `sql:"type:json" json:"amenities"`
	Description string    `json:"description"`
	RoomCount   int       `json:"room_count"`
	Currency    string    `json:"currency"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

type Amenities []string

func (sla *Amenities) Scan(src interface{}) error {
	return json.Unmarshal(src.([]byte), &sla)
}

func (sla Amenities) Value() (driver.Value, error) {
	val, err := json.Marshal(sla)
	return string(val), err
}
