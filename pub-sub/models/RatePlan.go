package models

import (
	"database/sql/driver"
	"encoding/json"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type RatePlan struct {
	HotelID            string             `json:"hotel_id"`
	RatePlanID         string             `gorm:"primaryKey" json:"rate_plan_id"`
	CancellationPolicy CancellationPolicy `sql:"type:json" json:"cancellation_policy"`
	Name               string             `json:"name"`
	OtherConditions    OtherConditions    `sql:"type:json" json:"other_conditions"`
	MealPlan           string             `json:"meal_plan"`
	CreatedAt          time.Time
	UpdatedAt          time.Time
}

type CancellationPoli struct {
	Type              string `json:"type"`
	ExpiresDaysBefore int    `json:"expires_days_before"`
}

type OtherConditions []string
type CancellationPolicy []CancellationPoli

func (sla *OtherConditions) Scan(src interface{}) error {
	return json.Unmarshal(src.([]byte), &sla)
}

func (sla OtherConditions) Value() (driver.Value, error) {
	val, err := json.Marshal(sla)
	return string(val), err
}

func (sla *CancellationPolicy) Scan(src interface{}) error {
	return json.Unmarshal(src.([]byte), &sla)
}

func (sla CancellationPolicy) Value() (driver.Value, error) {
	val, err := json.Marshal(sla)
	return string(val), err
}

// DBMigrate will create and migrate the tables, and then make the some relationships if necessary
func DBMigrate(db *gorm.DB) *gorm.DB {
	db.AutoMigrate(&RatePlan{})
	db.AutoMigrate(&Room{})
	db.AutoMigrate(&Hotel{})
	return db
}
