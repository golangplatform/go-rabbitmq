package models

import (
	"database/sql/driver"
	"encoding/json"
	"time"

	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Room struct {
	RoomID      string   `gorm:"primaryKey" json:"room_id"`
	HotelID     string   `json:"hotel_id"`
	Description string   `json:"description"`
	Name        string   `json:"name"`
	Capacity    Capacity `sql:"type:json" json:"capacity"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

type TCapacity struct {
	MaxAdults     int `json:"max_adults"`
	ExtraChildren int `json:"extra_children"`
}

type Capacity TCapacity

func (sla *Capacity) Scan(src interface{}) error {
	return json.Unmarshal(src.([]byte), &sla)
}

func (sla Capacity) Value() (driver.Value, error) {
	val, err := json.Marshal(sla)
	return string(val), err
}
