package routes

import (
	"fmt"
	"log"
	"net/http"
	"pub-sub/configs"
	"pub-sub/handlers"
	"pub-sub/models"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

type App struct {
	Router *mux.Router
	DB     *gorm.DB
}

// App initialize with predefined configuration
func (a *App) Initialize(config *configs.Config) {
	dbURI := fmt.Sprintf("%s:%s@(localhost)/%s?charset=%s&parseTime=True",
		config.DB.Username,
		config.DB.Password,
		config.DB.Name,
		config.DB.Charset)

	db, err := gorm.Open(config.DB.Dialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}

	a.DB = models.DBMigrate(db)
	a.Router = mux.NewRouter()
	a.setRouters()
}

// Set all required routers
func (a *App) setRouters() {
	// Routing for handling the projects
	a.Get("/rate-plan", a.GetRatePlans)
	a.Post("/rate-plan", a.AddRatePlan)
	a.Get("/hotel", a.GetHotels)
	a.Post("/hotel", a.AddHotel)
	a.Get("/room", a.GetRooms)
	a.Post("/room", a.AddRoom)

}

// Wrap the router for GET method
func (a *App) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("GET")
}

// Wrap the router for POST method
func (a *App) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("POST")
}

// Wrap the router for PUT method
func (a *App) Put(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("PUT")
}

// Wrap the router for DELETE method
func (a *App) Delete(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("DELETE")
}

// Handlers to manage rate plan data
func (a *App) GetRatePlans(w http.ResponseWriter, r *http.Request) {
	handlers.GetRatePlans(a.DB, w, r)
}

func (a *App) AddRatePlan(w http.ResponseWriter, r *http.Request) {
	handlers.AddRatePlan(a.DB, w, r)
}

// Handlers to manage hotel data
func (a *App) GetHotels(w http.ResponseWriter, r *http.Request) {
	handlers.GetHotels(a.DB, w, r)
}

func (a *App) AddHotel(w http.ResponseWriter, r *http.Request) {
	handlers.AddHotel(a.DB, w, r)
}

// Handlers to manage room data
func (a *App) GetRooms(w http.ResponseWriter, r *http.Request) {
	handlers.GetRooms(a.DB, w, r)
}

func (a *App) AddRoom(w http.ResponseWriter, r *http.Request) {
	handlers.AddRoom(a.DB, w, r)
}

// Run the app on it's router
func (a *App) Run(host string) {
	log.Fatal(http.ListenAndServe(host, a.Router))
}
